import { Orders } from "acosor-services";

const createOrder = async (ctx: any, next: any) => {
  const { id, ref, itemCount, orderTotal, status } = ctx.request.body;
  const createdBy = "WOW";

  const cmd = Orders.Commands.CreateNew(
    {
      aggregateID: id,
      itemCount,
      orderTotal,
      ref,
      status,
    },
    createdBy,
  );

  await Orders.Service.dispatch(cmd);

  ctx.body = {
    data: {
      _id: id,
      _version: 1,
      ref,
      status,
    },
    success: true,
  };

  next();
};

export default createOrder;
