import * as Router from "koa-router";
import createOrder from "./createOrder";

export const mapRoutes = (router: Router) => {
  router.post("/poz/orders", createOrder);
};
