import * as Router from "koa-router";

import { mapRoutes as poz } from "./poz/mapRoutes";
import { mapRoutes as pss } from "./pss/mapRoutes";

export const mapApp = (app: any) => {
  const router = new Router({ prefix: "/api" });

  poz(router);
  pss(router);

  app
    .use(router.routes())
    .use(router.allowedMethods());
};
