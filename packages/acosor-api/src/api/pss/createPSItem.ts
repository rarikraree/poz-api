import { PSS } from "acosor-services";

const createPSItem = async (ctx: any, next: any) => {
  const { id, barcode, price, sku, title, type, vendor } = ctx.request.body;
  const createdBy = "WOW";

  const cmd = PSS.Commands.CreateNew(
    {
      aggregateID: id,
      barcode,
      price,
      sku,
      title,
      type,
      vendor,
    },
    createdBy,
  );

  await PSS.Service.dispatch(cmd);

  ctx.body = {
    data: {
      _id: id,
      _version: 1,
      barcode,
      price,
      sku,
      title,
      type,
      vendor,
    },
    success: true,
  };

  next();
};

export default createPSItem;
