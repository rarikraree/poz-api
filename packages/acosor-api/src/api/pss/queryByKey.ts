import { PSS } from "acosor-services";

const queryByKey = async (ctx: any, next: any) => {
  const { query } = ctx.request.body;

  ctx.body = await PSS.Queries.ByKey({ key: query });

  next();
};

export default queryByKey;
