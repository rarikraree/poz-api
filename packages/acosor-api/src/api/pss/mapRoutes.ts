import * as Router from "koa-router";
import createPSItem from "./createPSItem";
import queryByKey from "./queryByKey";

export const mapRoutes = (router: Router) => {
  router.post("/pss/query", queryByKey);
  router.post("/pss/item", createPSItem);
};
