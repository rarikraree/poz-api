import * as cors from "@koa/cors";
import { Orders, PSS } from "acosor-services";
import * as Koa from "koa";
import * as bodyParser from "koa-bodyparser";
import * as logger from "koa-logger";

import { mapApp } from "./api/mapApp";

(async () => {
  await PSS.Service.connect();
  await Orders.Service.connect();

  const app = new Koa();

  app.use(logger());
  app.use(cors());
  app.use(bodyParser());

  mapApp(app);

  // tslint:disable-next-line: no-console
  app.listen(8080, () => console.log("app listening at 8080"));
})();
