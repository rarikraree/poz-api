import { IEventStores } from "atomservices";
import { createEventStores, IEventStoresConnector } from "atomstores-mongo";
import { Events } from "../DataStores/core/DataContexts";

const connector: IEventStoresConnector = {
  connect: async (scope, type) => {
    const repository = Events.toRepository(type);

    return repository.collection();
  },
};

export const EventStores: IEventStores = createEventStores(connector);
