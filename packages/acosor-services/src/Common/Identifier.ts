import { IIdentifier } from "atomservices";
import * as UUID from "uuid";

export const Identifier: IIdentifier = {
  AggregateID: (type) => UUID.v4(),
  EventID: (type) => UUID.v4(),
};
