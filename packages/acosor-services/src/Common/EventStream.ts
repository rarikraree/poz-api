import { IEventStream } from "atomservices";
import { createEventStream } from "atomstream-rbmq";

export const EventStream: IEventStream = createEventStream();
