import { DataStores, IDataContext, initialize } from "archdatastores";
import * as MongoDB from "mongodb";

initialize({
  Events: {
    config: {
      database: "acosor-events",
      host: "localhost",
      port: 27017,
    },
    connector: "#mongodb",
  },
  States: {
    config: {
      database: "acosor-states",
      host: "localhost",
      port: 27017,
    },
    connector: "#mongodb",
  },
});

export const Events: IDataContext<MongoDB.Db, MongoDB.Collection> = DataStores.resolve("Events");

export const States: IDataContext<MongoDB.Db, MongoDB.Collection> = DataStores.resolve("States");
