import { Command as CreateNew } from "./CreateNew";

const Commands = {
  CreateNew,
};

Object.freeze(Commands);

export default Commands;
