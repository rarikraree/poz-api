import { Commands, IAtomCommand } from "atomservices";
import { Type } from "../Configs";
import { INewCreatedEvent, Name as EventName } from "../Events/NewCreated";

const Name = "CreateNew";

interface ICreateNewCommandPayloads {
  aggregateID?: string;
  barcode?: string;
  sku: string;
  type: string;
  title: string;
  vendor: string;
  price: number;
}

export interface ICreateNewCommand extends IAtomCommand<ICreateNewCommandPayloads> {
}

export const Command = (
  data: {
    aggregateID?: string;
    barcode?: string;
    sku: string;
    type: string;
    title: string;
    vendor: string;
    price: number;
  },
  createdBy: string,
) =>
  Commands.Builders.CommandBuilder<ICreateNewCommand, ICreateNewCommandPayloads>(
    {
      createdBy,
      name: Name,
    },
    data,
  );

export const CommandHandler = Commands.Builders.CommandHandlerBuilder<ICreateNewCommand, INewCreatedEvent>({
  name: Name,
  transform: (command, identifier) => {
    const { aggregateID, barcode, price, sku, title, type, vendor } = command.payloads;

    const event = {
      _createdAt: new Date(),
      _createdBy: command._createdBy,
      _id: identifier.EventID(),
      _version: 1,
      aggregateID: aggregateID || identifier.AggregateID(),
      name: EventName,
      payloads: {
        barcode,
        price,
        sku,
        title,
        type,
        vendor,
      },
      type: Type,
    };

    return event;
  },
  validate: (command) => {
    return {
      isValid: true,
    };
  },
});
