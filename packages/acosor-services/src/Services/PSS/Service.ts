import { Services } from "atomservices";
import { EventStores, EventStream, Identifier } from "../../Common";
import Configs from "./Configs";

import { CommandHandler as CreateNew } from "./Commands/CreateNew";

import { EventHandler as NewCreated } from "./Events/NewCreated";

const CommandHandlers = [CreateNew];

const EventHandlers = [NewCreated];

const Service = Services.createService(
  Identifier,
  EventStream,
  Configs,
  {
    EventStores,
  },
)(
  {
    CommandHandlers,
    EventHandlers,
  },
);

export default Service;
