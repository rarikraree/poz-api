import { IAtomState } from "atomservices";

export interface IPSState extends IAtomState {
  sku: string;
  type: string;
  title: string;
  price: number;
  vendor: string;
  barcode?: string;
}
