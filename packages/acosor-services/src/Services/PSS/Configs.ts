import { IServiceConfigs } from "atomservices";
import { Scope } from "../../Scope";

export const Type = "PSS";

const Configs: IServiceConfigs = {
  levels: {
    _default: "Public",
  },
  scope: Scope,
  type: Type,
};

export default Configs;
