import { Events, IAtomEvent, States } from "atomservices";
import { PSS } from "../../../DataStores/PSS";
import { IPSState } from "../IPSState";

export const Name = "NewCreated";

interface INewCreatedEventPayloads {
  sku: string;
  type: string;
  title: string;
  price: number;
  vendor: string;
  barcode?: string;
}

export interface INewCreatedEvent extends IAtomEvent<INewCreatedEventPayloads> {
}

export const EventHandler = Events.Builders.EventHandlerBuilder<INewCreatedEvent>({
  name: Name,
  process: async (event) => {
    const { barcode, price, sku, title, type, vendor } = event.payloads;

    const item: IPSState = States.Builders.CreateStateBase<IPSState>(event);
    item.price = price;
    item.sku = sku;
    item.title = title;
    item.vendor = vendor;
    item.type = type;

    if (barcode) {
      item.barcode = barcode;
    }

    PSS.createItem(item);
  },
});
