import { PSS } from "../../../DataStores/PSS";

export interface IQueryData {
  key: string;
}
export interface IQueryResult {
  data?: {
    type: string,
    item: any;
  };
  success: boolean;
}

export const ByKey = async (data: IQueryData): Promise<IQueryResult> => {
  const { key } = data;
  const collection = await PSS.collection();
  const ps = [collection.findOne({ sku: key }), collection.findOne({ barcode: key })];
  const [sku, barcode] = await Promise.all(ps);

  if (sku) {
    return {
      data: {
        item: sku,
        type: "sku",
      },
      success: true,
    };
  }

  if (barcode) {
    return {
      data: {
        item: barcode,
        type: "barcode",
      },
      success: true,
    };
  }

  return {
    success: true,
  };
};
