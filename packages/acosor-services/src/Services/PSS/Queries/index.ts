import { ByKey } from "./ByKey";

const Queries = {
  ByKey,
};

Object.freeze(Queries);

export default Queries;
