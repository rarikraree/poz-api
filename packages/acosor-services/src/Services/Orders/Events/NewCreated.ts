import { Events, IAtomEvent, States } from "atomservices";
import { Orders } from "../../../DataStores/Orders";
import { IOrderState } from "../IOrderState";

export const Name = "NewCreated";

interface INewCreatedEventPayloads {
  ref: string;
  status: "transaction" | "payment" | "delivery";
  itemCount: number;
  orderTotal: number;
}

export interface INewCreatedEvent extends IAtomEvent<INewCreatedEventPayloads> {
}

export const EventHandler = Events.Builders.EventHandlerBuilder<INewCreatedEvent>({
  name: Name,
  process: async (event) => {
    const { itemCount, orderTotal, ref, status } = event.payloads;

    const item: IOrderState = States.Builders.CreateStateBase<IOrderState>(event);
    item.itemCount = itemCount;
    item.orderTotal = orderTotal;
    item.ref = ref;
    item.status = status;

    Orders.createItem(item);
  },
});
