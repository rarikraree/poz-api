import { Commands, IAtomCommand } from "atomservices";
import { Type } from "../Configs";
import { INewCreatedEvent, Name as EventName } from "../Events/NewCreated";

const Name = "CreateNew";

interface ICreateNewCommandPayloads {
  aggregateID: string;
  ref: string;
  status: "transaction" | "payment" | "delivery";
  itemCount: number;
  orderTotal: number;
}

export interface ICreateNewCommand extends IAtomCommand<ICreateNewCommandPayloads> {
}

export const Command = (
  data: {
    aggregateID: string;
    ref: string;
    status: "transaction" | "payment" | "delivery";
    itemCount: number;
    orderTotal: number;
  },
  createdBy: string,
) =>
  Commands.Builders.CommandBuilder<ICreateNewCommand, ICreateNewCommandPayloads>(
    {
      createdBy,
      name: Name,
    },
    data,
  );

export const CommandHandler = Commands.Builders.CommandHandlerBuilder<ICreateNewCommand, INewCreatedEvent>({
  name: Name,
  transform: (command, identifier) => {
    const { aggregateID, itemCount, orderTotal, ref, status } = command.payloads;

    const event: INewCreatedEvent = {
      _createdAt: new Date(),
      _createdBy: command._createdBy,
      _id: identifier.EventID(),
      _version: 1,
      aggregateID: aggregateID || identifier.AggregateID(),
      name: EventName,
      payloads: {
        itemCount,
        orderTotal,
        ref,
        status,
      },
      type: Type,
    };

    return event;
  },
  validate: (command) => {
    return {
      isValid: true,
    };
  },
});
