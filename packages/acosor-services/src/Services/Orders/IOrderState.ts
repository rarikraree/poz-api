import { IAtomState } from "atomservices";

export interface IOrderState extends IAtomState {
  ref: string;
  status: "transaction" | "payment" | "delivery";
  itemCount: number;
  orderTotal: number;
}
